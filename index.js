var express = require('express')

var app = express();
var db = require('./db.js');
var bodyParser = require('body-parser');
app.use(bodyParser.json())

app.get('/', function (req, res) {

res.send('Hello World!')

})

app.get('/test',function(req,res){
	db.query("SELECT * FROM app1", function(err,result){
		if(!err){
			res.json(result.rows);
		}
		else{
			res.send("Error");
			console.log(err)
		}
	});
})

app.get('/test/:code' , function(req,res){
	db.query("SELECT * FROM record where app_code=$1",[req.params.code], function(err,result){
	if(result.rows.length>0){
		res.json(result.rows);	
	}else{
		res.send("Error 404: No existeix");
		console.log(err);
	}
    });

});

app.post('/test/:code' , function(req,res){
	db.query("INSERT INTO record(id, app_code, player, socore) VALUES ($1,$2,$3,$4)",[req.body.id, req.params.code, req.body.player, req.body.socore], function(err,result){
	if(!err){
		res.json(result.rows);	
		console.log(req.body.content); 

	}else{
		res.send("Error 500: No existeix");
		console.log(err);
	}
    });

});


app.listen(process.env.PORT || 3000, function () {

console.log('Example app listening on port 3000!')

})
